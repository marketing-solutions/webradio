package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Genre;
import model.Territory;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-09-18T16:19:26")
@StaticMetamodel(Stations.class)
public class Stations_ { 

    public static volatile SingularAttribute<Stations, Integer> id;
    public static volatile SingularAttribute<Stations, Genre> idGenre;
    public static volatile SingularAttribute<Stations, String> name;
    public static volatile SingularAttribute<Stations, Territory> idTerritory;
    public static volatile SingularAttribute<Stations, String> url;

}