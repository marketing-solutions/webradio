package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Stations;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-09-18T16:19:26")
@StaticMetamodel(Genre.class)
public class Genre_ { 

    public static volatile SingularAttribute<Genre, Integer> id;
    public static volatile CollectionAttribute<Genre, Stations> stationsCollection;
    public static volatile SingularAttribute<Genre, String> name;

}