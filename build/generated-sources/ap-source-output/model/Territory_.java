package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Stations;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-09-18T16:19:26")
@StaticMetamodel(Territory.class)
public class Territory_ { 

    public static volatile SingularAttribute<Territory, Integer> id;
    public static volatile CollectionAttribute<Territory, Stations> stationsCollection;
    public static volatile SingularAttribute<Territory, String> name;

}