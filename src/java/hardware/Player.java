/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hardware;

import com.pi4j.component.lcd.impl.I2CLcdDisplay;
import com.pi4j.component.lcd.LCD;
import com.pi4j.component.lcd.LCDBase;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import java.lang.ProcessBuilder;
import java.lang.Process;    
import java.io.IOException;
import java.io. InputStreamReader;

public class Player {

	private final LCD lcd ;
	private final A6276 leddriver;
	private final GpioController gpiofactory;
	private final java.lang.Runtime rt;
	private java.lang.Process p;
	private int volume ;
	
	public Player () {
		this.lcd = new  I2CLcdDisplay(2,16,0,0x27,3,0,1,2,7,6,5,4);
		this.gpiofactory = new GpioFactory.getInstance();
		this.rt = java.lang.Runtime.getRuntime();
		this.leddriver = new A6276();
		this.volume = 50; 
	
	}
	public setVolume (int vol) {
		if ( vol>49 && vol < 101) { 
			p = rt.exec("mpc volume "+ Integer.toString(vol));
			this.volume = vol;
		}
		
	}
	
	public int getVolume () {
		return volume;
	}
	
	
	public void displayStatus () {
		p = rt.exec("mpc status");
        
        p.waitFor();
        
        
        is = p.getInputStream();
        reader = new java.io.BufferedReader(new InputStreamReader(is));
            
        String s = null;
		int i = 0;
		String track = "";
		String volume = "";

        while ((s = reader.readLine()) != null) {
			
			if (i == 0){ track = s; }
			if (i == 2){ 
			int ind = s.indexOf("repeat:");
			volume = s.substring(7,ind);
			}
			i++;

		}
		lcd.setCursorPosition(0,0);
		lcd.write(track);
		lcd.setCursorPosition(1,0);
		lcd.write("Volume: "+volume);
        is.close();
	}
	
	public void displayTemperature () {
	
	
	}
	
	public void displayHumidity () {
		
	
	}
	
	public void displayTrack () {
	
	
	}
	
	public void displayTime () {
		
	
	}
	
	public void displayVolume () {
		int leds = volume - 50 /10; 
		this.ledriver.displayLeds(leds);
	}
	
	
	
}
