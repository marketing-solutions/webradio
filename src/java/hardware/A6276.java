/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hardware;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class A6276 {
	private final GpioPinDigitalOutput pindout ;
	private final GpioPinDigitalOutput pinclk;
	private final GpioPinDigitalOutput pinlatch;
	private int val;
	private final GpioController gpio;
	
	
	
public A6276 () {
	this.gpio = GpioFactory.getInstance();
	this.pindout = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyLEDdout", PinState.LOW);
	this.pinclk = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "MyLEDclock", PinState.LOW);
	this.pinlatch = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "MyLEDlatch", PinState.LOW);
	this.val = 0x0000;
	shift_out_16(this.val);
}

public void setVal (int val) {
	this.val = val;
}

public int getVal () {
	return val;
}
public void ledplus () {
 val = (val << 1) | 0x01;
 //System.out.println(val);
 shift_out_16(val);
}

public void ledminus () {
val = (val >> 1) | 0x00;
 shift_out_16(val);

}
public void displayLeds(int v) {
	val = 0x0000;
	for (int i; i < v; v++)
	{
		ledplus();
	
	}

}

public void shift_out_16(int v) {
	int n;
   pinclk.low();   // be sure CLK is idle at zero
   pinlatch.high();
   try {
   for (n=0; n<16; n++)
   {
       if ((v >> (15 - n)) >= 1 )  // most sig bit first
       {
           pindout.high();
       }
       else
       {
           pindout.low();
       }

       Thread.sleep(1);
       pinclk.high();
       Thread.sleep(1);
       pinclk.low();
   }
   }
   catch (Exception e)
   {}
   pinlatch.low();
   pinlatch.high();



}

}
